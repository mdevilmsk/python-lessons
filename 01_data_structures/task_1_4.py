# -*- coding: utf-8 -*-
'''
Задание 1.4

Даны 3 переменные name age city

программа должна выводить строку:

Hello! My name is Ivan and Im 25 years old. Im from Moscow.

Напишите вывод строки минимум тремя способами
'''

name = 'Ivan'
age = '25'
city = 'Moscow'

fstring = f"Hello! My name is {name} and I'm {age} years old. I'm from {city}"
print(fstring)


# Хорошо
# Но в задании просилось тремя способами высести текст
# Вот еще варианты:

print('Hello! My name is', name , 'and Im', age, 'years old. Im from', city +'.')
print('Hello! My name is %s and Im %s years old. Im from %s.' % (name,age,city))
print('Hello! My name is {0} and Im {1} years old. Im from {2}.'.format(name, age, city))