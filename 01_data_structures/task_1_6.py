# -*- coding: utf-8 -*-
'''
Задание 1.6

Обработать строку vova и вывести информацию на стандартный поток вывода в виде:
name:                  Владимир
ip:                    10.0.13.3
город:                 Moscow
date:                  15.03.2020
time:                  15:20

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

vova = 'O Владимир       15.03.2020 15:20 10.0.13.3, 3d18h, Moscow/5'
userInfo = vova.strip().replace(', ', '_').replace('  ', '').replace(' ', '_').replace('/', '_').split('_')
print(f"""
name:   {userInfo[1]}
ip:     {userInfo[4]}
город:  {userInfo[6]}
date:   {userInfo[2]}
time:   {userInfo[3]}
""")


# Отлично!