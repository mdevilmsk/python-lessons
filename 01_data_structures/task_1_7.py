# -*- coding: utf-8 -*-
'''
Задание 1.7

Дано число n

Подсчитать и вывесети на экран сумму эго цифр

Вариант для продвинутых:
Дополнительно написать код, что 5ти значное число n - задается с клавиатуры пользователем
'''

n = 65142

num = f'{n}'
print(int(num[0]) + int(num[1]) + int(num[2]) + int(num[3]) + int(num[4]))

# Отлично!

print('input 5 digits')
n = input()
if n.isdigit():
    num = f'{n}'
    print(int(num[0]) + int(num[1]) + int(num[2]) + int(num[3]) + int(num[4]))
else:
    print("wrong input")
