# -*- coding: utf-8 -*-
'''
Задание 2.1b

Переделать скрипт из задания 2.1a таким образом, чтобы, при запросе параметра,
отображался список возможных параметров. Список параметров надо получить из словаря,
а не прописывать вручную.

Вывести информацию о соответствующем параметре, указанного устройства.

Пример выполнения скрипта:
$ python task_2_1b.py
Введите имя сервера: dev
Введите имя параметра (os, model, vendor, location, ip): ip
10.255.0.1

Ограничение: нельзя изменять словарь london_co.

Все задания надо выполнять используя только пройденные темы.
То есть эту задачу можно решить без использования условия if.
'''

servers = {
    'dev': {
        'location': 'Лубянка',
        'vendor': 'IBM',
        'model': 'i2570',
        'os': 'centos',
        'ip': '10.255.0.1'
    },
    'qa': {
        'location': 'поеображенка',
        'vendor': 'Cisco',
        'model': '4451',
        'os': 'centos',
        'ip': '10.255.0.2'
    },
    'prod': {
        'location': 'технопарк',
        'vendor': 'Dell',
        'model': '3850',
        'os': 'centos',
        'ip': '10.255.0.101',
        'vlans': '10,20,30',
        'routing': True
    }
}

print('Введите имя сервера:')
inputVal = input().split()
if len(inputVal) < 1:
    print('usage python task_2_1a <server type>')
    exit(-1)

server = servers.get(inputVal[0])
print(f'Введите параметр {list(server.keys())}:')
paramInput = input()
if server:
    param = server.get(paramInput)
    print(param)
else:
    print('no such server')

