"""
Задание 5.3

В прошлом задании для вывода на экран данных о каждом объекте мы писали отдельный код 
        для каждого объекта. Это громоздко и нерационально, ООП позволяет оптимизировать код.
Скопируйте класс из предыдущего задания и создайте метод show_contact(), который будет выводить данные 
    любого объекта типа Contact в том же виде, как сейчас их выводит функция print_contact.
В теле класса Contact напишите метод show_contact, который в качестве параметра 
    будет принимать переменную self. В теле метода выполните print(), точно такой же, 
    как в функции print_contact, только вместо имени объекта в аргументе укажите self.
Скопируйте код инициализации объектов mike и vlad
Вызовите метод show_contact для объектов mike и vlad
Удалите из кода функцию print_contact().
Запустите код

"""


class Contact:
    def __init__(self, name: str, phone: str, address: str, birthday: str):
        self.name = name
        self.phone = phone
        self.address = address
        self.birthday = birthday

    def show_contact(self):
        print(f"""
            Имя: {self.name} 
            адрес: {self.address}
            телефон: {self.phone}
            день рождения: {self.birthday}
        """)


if __name__ == '__main__':
    mike = Contact(name="Михаил Булгаков", phone="4-08-30", address="Россия, Москва, Пироговская, дом 40, кв. 60",
                   birthday="15.05.1891")
    vlad = Contact(name="Владимир Маяковский", phone="33-78-40",
                   address="Россия, Москва, Лубянская улица, д. 10, кв. 31", birthday="19.07.1893")

    mike.show_contact()
    print("----------------------")
    vlad.show_contact()
