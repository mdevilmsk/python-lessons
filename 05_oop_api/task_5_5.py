"""
Задание 5.5 (по желанию)

Для тех у кого доступер api.telegram.org или те кто могут сделать его доступным
    при помощи доп средств (vpn / proxy)
Создайте telegram бота в боте @BotFather

Напишите скрипт, который при выполнении - отправит сообщение от ващего бота в чат.

В идеале использовать библиотеку requests для python

Вариант для ленивых - использовать curl из python скрипта.

ВАЖНО!
- Пишите мне, чтобы добавить вашего бота и вас в тестовую группу
- chat_id куда отправлять сообщение - я пришлю в личку при обращении
- не комитьте код бота в с токеном и chat_id на gitlab
"""

import requests
from bot import bot_key
import json

if __name__ == "__main__":

    updates = requests.get(f'https://api.telegram.org/bot{bot_key()}/getUpdates')
    data = json.loads(updates.text)
    send_message = ""
    for message in data.get("result"):
        text = message.get("message").get("text")
        if text:
            send_message = text

    r = requests.post(f'https://api.telegram.org/bot{bot_key()}/sendMessage',
                      json={"chat_id": "-429898709", "text": "hello from bot, message is : " + send_message})
    print(r.content)
