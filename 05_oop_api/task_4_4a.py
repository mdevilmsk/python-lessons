"""
Здание 4.4a
Cкопируйте функции написаные в предыдущем задании
Написать функции:

validate_operator(operator)
    функция проверяет, что operator равен допустимым значениям
        допустимые значения: + - * /
    Если оператор принимает допустимое значение:
        возвращается True
    Если оператор НЕ принимает допустимое значение:
        выводится строка "Неизвестный оператор"
        И возвращается False

validate_value(val, operator)
    Функция проверяет что значение val либо int либо float
    Если все верно - возвращается True
    Если оператор равен "/" а второй операнд равен нулю:
        Выводится строка: "На ноль делить нельзя!"
        И возвращается - False
    В остальных случаях:
        Выводится строка: "Неподдерживаемый тип опреранда"
        И возвращается - False
    
calculator(v1 :int or float , operator :str, v2 :int or float)

При запуске функции:
    Проверяются оператор и операнды функциями:
        validate_operator
        validate_value
В зависимости от оператора выплнить соответствующую функцию из предыдучего задания
    Результат выполнения записать в переменную result и вернуть в конце
"""


def plus_args(v1, v2):
    value = v1 + v2
    print(f'Операция - сложение: {v1} + {v2}')
    return value


def minus_args(v1, v2):
    value = v1 - v2
    print(f'Операция - вычитание: {v1} - {v2}')
    return value


def multi_args(v1, v2):
    value = v1 * v2
    print(f'Операция - умножение: {v1} * {v2}')
    return value


def div_args(v1, v2):
    if v2 == 0 and v1 != 0:
        value = "~∞ (комплексная бесконечность)"
    else:
        value = "неопределено" if v2 == 0 and v1 == 0 else v1 / v2
    print(f'Операция - деление: {v1} / {v2}')
    return value


def validate_operator(operator):
    operations = ["+", "-", "*", "/"]
    if operator not in operations:
        print("Неизвестный оператор")
        return False
    return True


def validate_value(val, operation):
    if not isinstance(val, int) and not isinstance(val, float):
        print("Неподдерживаемый тип опреранда")
        return False
    if operation == "/" and val == 0:
        print("На ноль делить нельзя!")
        return False
    return True


def calculator(v1: int or float, operator: str, v2: int or float):
    if isinstance(operator, str):
        operator = operator.strip()
    if validate_operator(operator) and validate_value(v2, operator):
        value = plus_args(v1, v2) if operator == "+" \
            else minus_args(v1, v2) if operator == "-" \
            else multi_args(v1, v2) if operator == "*" \
            else div_args(v1, v2)
        return value
    return False


if __name__ == '__main__':
    print(calculator(12.3, "/", 1))
