"""
Здание 4.4

Написать 4 функции
plus_args(v1, v2)   # для сложения aргументов v1 + v2
minus_args(v1, v2)  # для вычетания aргументов v1 - v2
multi_args(v1, v2)  # для умножения аргументов v1 * v2
div_args(v1, v2)    # для деления аргументов v1 / v2

пример запуска функции plus_args :

print(plus_args(5, 9))
Операция - сложение: 5 + 9
14
"""


def plus_args(v1, v2):
    value = v1 + v2
    print(f'Операция - сложение: {v1} + {v2}')
    return value


def minus_args(v1, v2):
    value = v1 - v2
    print(f'Операция - вычитание: {v1} - {v2}')
    return value


def multi_args(v1, v2):
    value = v1 * v2
    print(f'Операция - умножение: {v1} * {v2}')
    return value


def div_args(v1, v2):
    if v2 == 0 and v1 != 0:
        value = "~∞ (комплексная бесконечность)"
    else:
        value = "неопределено" if v2 == 0 and v1 == 0 else v1 / v2
    print(f'Операция - деление: {v1} / {v2}')
    return value

