# -*- coding: utf-8 -*-
'''
Задание 4.1

Обработать строки из файла servers.txt
и вывести информацию по каждому серверу в таком виде:

server:            qa_test1
ip:                10.0.13.3
switch_port:       FastEthernet0/0
uptime:            3d20h

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''

if __name__ == '__main__':
    server_file = open('servers.txt')
    for line in server_file:
        server = line.replace(',', '').split()
        print(f'server: {server[1]}\nip: {server[4]}\nswitch_port: {server[6]}\nup time: {server[5]}')
        print('-------------------------------------------------------------------------------------')
