"""
Задание 4.2
Напишите функцию, которая проверяет является ло передаваемый ей год високосным или нет.
      'На вход подается год. 
      Функция должна возвращать:
        True - високосный, 
        False - обычный'
"""


def check_year(year_num):
    if int(year_num) % 4 == 0:
        return True
    else:
        return False


if __name__ == '__main__':
    year = input("введите год: ")
    if not year.isdigit():
        print("год должен содержать только цифры")
        exit()
    if check_year(year):
        print("високосный")
    else:
        print("обычный")
