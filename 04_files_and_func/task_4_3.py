"""
Задание 4.3
Напишите функцию, которая вычисляет количество дней в месяце.
    На вход подается номер месяца и параметр, который указывает, является ли год високосным.

"""


def check_year(in_year):
    if int(in_year) % 4 == 0:
        return True
    else:
        return False


def get_month_days_num(in_year, in_month):
    thirty_one = [1, 3, 5, 7, 8, 10, 12]
    thirty = [4, 6, 9, 11]
    if in_month in thirty_one:
        return 31
    elif month in thirty:
        return 30
    else:
        return 28 + (1 if check_year(in_year) else 0)


if __name__ == '__main__':
    input_val = input("введите год и месяц через пробел: ").split()
    if len(input_val) < 2:
        print("нужно ввести два числа")
        exit()

    year = input_val[0]
    month = input_val[1]
    if not year.isdigit() or not month.isdigit() or int(month) <= 0 or int(month) > 12:
        print("год и месяц должны содержать только цифры, месяц должен быть от 1 до 12")
        exit()
    month = int(month)
    year = int(year)
    print(f'количество дней {get_month_days_num(year, month)}')
