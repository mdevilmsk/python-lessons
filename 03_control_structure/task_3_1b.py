'''
Задание 3.1b
Скопировать код из предыдущего задания


Допишите код, чтобы:
    - после вывода результата код запускался заново и опять запрашивал ввод выражения;
    - при вводе пустой строки программа останавливалась.

'''
if __name__ == '__main__':
    operations = ["+", "-", "*", "/"]
    while True:
        input_val = input("Введите выражение или нажмите Enter чтобы выйти: ")
        if input_val == "":
            break
        found = False

        for i in operations:
            if input_val.__contains__(i):
                found = True
                values = input_val.split(i)
                x = int(values[0])
                y = int(values[1])
                if i == "+":
                    print(f"Результат сложения: {x + y}")
                elif i == "-":
                    print(f" Результат вычитания: {x - y}")
                elif i == "*":
                    print(f" Результат умножения: {x * y}")
                elif i == "/":
                    print(f" Результат деления: {x / y}")
                break

        if not found:
            print("Невалидное выражение")
