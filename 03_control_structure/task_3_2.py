'''
Задание 3.2

print('Простой калькулятор вклада. На вход подаются следующие параметры: сумма вклада в рублях 
      '(число, может быть с копейками),\n'
      'срок вклада (число дней), процентная ставка (число, может быть с плавающей точкой).\n'
      'Для выхода из программы нужно в любой из параметров ввести exit\n')
print('Формула расчета вклада: SR=S*P/100*D/365; S - первоначальная сумма денег, вложенная в банк;\n'
      'P - годовая процентная ставка; D - количество дней, за которое начисляется процент.\n')

1. Зпросить у пользователя ввод следующих данных:
    - Сумма вклада
    - Срок вклада (в днях)
    - Процентная ставка %
2. Проверить, что:
    - Сумма вклада > 0
    - Срок вклада > 30
    - Процентная ставка > 0
3. Посчитать доходность по вкладу по формуле ниже и вывести на экран

4. После выполнения расчета вывести на экран строку:
    "Произвести новый расчет? (да/нет)"
        - Если введено 'Да' или 'Д' - запустить код заново;
        - Если введено 'Нет' или 'Н' - выйти из выполнения программы.
        - Если введено что-то другое:
             - повторить запрос (да/нет).
        - Сделать ввод нечувствительным к регистру.

Формула расчета вклада: 
    Profit = S*P/100*D/365
    S - первоначальная сумма денег, вложенная в банк;
    P - годовая процентная ставка; 
    D - количество дней, за которое начисляется процент
'''

if __name__ == '__main__':
    print('Простой калькулятор вклада. На вход подаются следующие параметры: сумма вклада в рублях'
          '(число, может быть с копейками),\n'
          'срок вклада (число дней), процентная ставка (число, может быть с плавающей точкой).\n'
          'Для выхода из программы нужно в любой из параметров вexвести exit\n')
    print('Формула расчета вклада: SR=S*P/100*D/365; S - первоначальная сумма денег, вложенная в банк;\n'
          'P - годовая процентная ставка; D - количество дней, за которое начисляется процент.\n')
    while True:
        input_sum = input("Сумма вклада: ")
        if input_sum == "exit":
            break
        elif not input_sum.isnumeric() or int(input_sum) <= 0:
            print("неверно введена сумма")
            continue
        input_term = input("Срок вклада: ")
        if input_term == "exit":
            break
        elif not input_term.isnumeric() or int(input_term) <= 30:
            print("неверно введен срок вклада")
            continue
        input_percent = input("Процентная ставка: ")
        if input_percent == "exit":
            break
        elif not input_percent.isnumeric() or int(input_percent) <= 0:
            print("неверно введена процентная ставка")
            continue
        found = False

        summary = int(input_sum)
        term = int(input_term)
        percent = int(input_percent)
        profit = summary*percent/100*term/365
        print(f"Расчетная доходность: {profit}")
        answer = False
        while not answer:
            retry = input("повторить расчет? (да/нет): ").lower()
            if retry == "да" or retry == "д":
                break
            elif retry == "нет" or retry == "н":
                exit(0)
